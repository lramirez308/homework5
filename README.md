Lorena Ramirez

1. See logger.log, why is it different from the log to console?

There is difference from the log to the console by 2 lines that are displayed i the log that are not shown in the console.

2. Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

The FINER error is being printed to standard error, therefore not being logged  into the console since it only prints standard out.

3. What does Assertions.assertThrows do?

Assertions.assertThrows() throws an exception of expected type and returns an   exception that it was expecting. The method will only work if the exception     thrown is what is expected and will fail if there is no exception thrown or if  it is an exception that it is not expecting.

4. See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)

	A serialVersionUID is a serialization runtime associated with each serializable class and provides it a version number. It is necessary to have a serialVersionUID so that the loaded classes for an object to be compatible with that respective serialization. If it is not compatible, that would result in an InvalidClassException. If one is not explicitly declared, then the serialization runtime will calculate a default value.

    2.  Why do we need to override constructors?

	We need to override the constructors to add in different parameters.

    3.  Why we did not override other Exception methods?

	We did not override other Exception methods because we only need to override our own expection class and not modify the others.

5. The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

The static block is opening a file named logger.properties and if it does not succeed, then it will catch the exception and print out an error that the file could not open and the logging was not configured.

6. What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

A README.md file is a markdown for formatting text. On bitbucket, if the README.md file is in the root level, it will display the contents of the file on the Overview page of the repository and renders the markdowns of the README.md file.

7. Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

The test was failing because the timeNow was being set in a try block. We need to move the timeNow outside the try block.

8. What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

The issue was that the timeNow being set in the try block caused a NullPointerException when it was testing for a time failed at runtime. The sequence of the exceptions and handlers is in a stack trace sequence and when there is an exception thrown, the exception class terminates the debugging and runtime of the program.

9. Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

on canvas submission

10. Make a printScreen of your eclipse Maven test run, with console

on canvas submission

11. What category of Exceptions is TimerException and what is NullPointerException

The TimerException is in the category of a RuntimeException. A NullPointerException is an exception that occurs when the user is trying to reference something in memory that is null and is a part of the Runtime Exception category.


12. Push the updated/fixed source code to your own repository.
